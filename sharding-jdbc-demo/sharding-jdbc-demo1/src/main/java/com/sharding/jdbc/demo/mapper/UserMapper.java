package com.sharding.jdbc.demo.mapper;


import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.sharding.jdbc.demo.model.User;

@Mapper
public interface UserMapper {

    public int insert(User user);
    
    public List<User> selectAll();

	public List<User> selectByAge(@Param("ageMin")Integer ageMin, @Param("ageMax")Integer ageMax);

	/**
	 * 分页查询
	 * @author zg
	 * @date 2020年4月15日 
	 * @param page
	 * @return
	 */
	//public List<User> selectByPage(Integer page);

}
