package com.sharding.jdbc.demo.model;

import java.util.Date;

/**
* @comments
* @author  zg
* @date 2018年12月2日
*/
public class User {

	private Long id;
	
	private String name;
	
	private Integer age;
	
	private Date crtTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Date getCrtTime() {
		return crtTime;
	}

	public void setCrtTime(Date crtTime) {
		this.crtTime = crtTime;
	}
	
	
}
