package com.sharding.jdbc.demo.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sharding.jdbc.demo.mapper.UserMapper;
import com.sharding.jdbc.demo.model.User;
import com.sharding.jdbc.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;

/**
* @comments
* @author  zg
* @date 2020年4月15日
*/
@Slf4j
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public List<User> queryList(Integer pageNum) throws Exception {
		Page<User> page = PageHelper.startPage(pageNum, 10);
		List<User> list = userMapper.selectAll();
		
		ObjectMapper om = new ObjectMapper();
		log.info("总记录数：{}, 总页数：{}", page.getTotal(), page.getPages());
		int listSize = list == null ? 0 :  list.size();
		log.info("当前第【{}】页，本页记录数：{}。\n返回数据：{}", page.getPageNum(), listSize, om.writeValueAsString(list));
		return list;
	}

}
