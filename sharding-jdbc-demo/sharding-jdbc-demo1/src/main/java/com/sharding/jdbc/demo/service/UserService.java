package com.sharding.jdbc.demo.service;

import java.util.List;

import com.sharding.jdbc.demo.model.User;

/**
* @comments
* @author  zg
* @date 2020年4月15日
*/
public interface UserService {

	/**
	 * 分页查询
	 * @author zg
	 * @date 2020年4月15日 
	 * @param pageNum
	 * @return
	 * @throws Exception 
	 */
	List<User> queryList(Integer pageNum) throws Exception;

}
