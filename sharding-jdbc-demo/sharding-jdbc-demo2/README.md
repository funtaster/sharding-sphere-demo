# sharding-sphere-demo

#### 项目介绍
Spring boot 2.3.7 + Sharding-Sphere 4.1.1

场景：随着用户数的增长（用户id自增长），用户产生的数据也越来越多。
此示例为了应对这种场景，根据用户id创建用户数据表，对用户id使用自义定分片策略，根据用户id增长量来创建用户数据表。




