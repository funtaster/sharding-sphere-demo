package junit;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.sharding.jdbc.demo2.mapper.PvMapper;
import com.sharding.jdbc.demo2.model.Pv;
import com.sharding.jdbc.demo2.service.UserService;

/**
* @comments
* @author  zg
* @date 2021年1月1日
*/
//@ActiveProfiles("dev") 
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ShardingJdbcDemo2Test.class)
@MapperScan(basePackages = {"com.sharding.jdbc.demo2.mapper"})
@SpringBootApplication(scanBasePackages = "com.sharding.jdbc.demo2")
public class ShardingJdbcDemo2Test {

    @Autowired
    private UserService userService;
    @Autowired
    private PvMapper pvMapper;
    
    @Test
    public void createUsers() {
        //创建用户，并根据用id的范围创建用户数据表
        userService.createUsers(57);
    }
    
    @Test
    public void randomUserVisit() {
        int maxUsersNum = 100; //最大用户数
        int visitTimes = 2;//访问次数
        userService.randomUserVisit(maxUsersNum, visitTimes);
    }
    
    @Test
    public void queryVisitLog() {
        int uid = 50;
        List<Pv> list = pvMapper.selectVisitLog(uid);
        System.out.println(JSON.toJSONString(list));
    }
    
}
