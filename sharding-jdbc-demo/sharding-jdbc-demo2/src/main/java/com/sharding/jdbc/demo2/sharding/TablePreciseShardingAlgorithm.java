package com.sharding.jdbc.demo2.sharding;

import java.util.Collection;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;
import org.apache.shardingsphere.api.sharding.standard.RangeShardingAlgorithm;

import com.alibaba.fastjson.JSON;
import com.sharding.jdbc.demo2.util.ShardingUtils;

import lombok.extern.slf4j.Slf4j;

/**
* @comments
* @author  zg
* @date 2021年1月1日
*/
@Slf4j
public class TablePreciseShardingAlgorithm implements PreciseShardingAlgorithm<Integer>{
    
    //public static int range = 5;

    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<Integer> shardingValue) {
        log.info("collection:{}", JSON.toJSONString(availableTargetNames));//表名
        log.info("preciseShardingValue:{}", JSON.toJSONString(shardingValue));//{"columnName":"uid","logicTableName":"pv","value":13}
        Integer uid = shardingValue.getValue();//得到uid
        String tableName = ShardingUtils.getShardingTableByRange("pv", ShardingUtils.pv_segmentSize, uid);
        /*if(!availableTargetNames.contains(tableName)) {
            log.warn("没有表，则创建{}", tableName);
        }*/
        
        return tableName;
    }

}
