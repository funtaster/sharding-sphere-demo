package com.sharding.jdbc.demo2.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* @comments
* @author  zg
* @date 2018年12月2日
*/
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class User {

	private Integer id;
	
	private String name;
	
	private Integer age;
	
	private Date crtTime;

}
