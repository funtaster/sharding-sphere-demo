package com.sharding.jdbc.demo2.mapper;


import java.util.List;

import com.sharding.jdbc.demo2.model.User;

public interface UserMapper {

    int insert(User user);

    List<User> selectUserList(int userCount);


}
