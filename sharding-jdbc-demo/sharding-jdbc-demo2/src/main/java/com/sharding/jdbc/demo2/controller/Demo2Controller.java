package com.sharding.jdbc.demo2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.sharding.jdbc.demo2.mapper.PvMapper;
import com.sharding.jdbc.demo2.model.Pv;
import com.sharding.jdbc.demo2.service.UserService;

import lombok.extern.slf4j.Slf4j;

/**
* @comments
* @author  zg
* @date 2021年1月2日
*/
@Slf4j
@RestController
@RequestMapping("demo2")
public class Demo2Controller {

    @Autowired
    private UserService userService;
    @Autowired
    private PvMapper pvMapper;
    
    @GetMapping("/user/create")
    public String createUser(@RequestParam(defaultValue = "100") Integer count) {
        log.info("创建{}个用户", count);
        userService.createUsers(count);
        return "OK";
    }
    
    @GetMapping("/user/visit")
    public String userVisit(@RequestParam(defaultValue = "100")int usersCount, @RequestParam(defaultValue = "50")int visitTimes) {
        log.info("用户访问 -> usersCount={},visitTimes={}", usersCount, visitTimes);
        userService.randomUserVisit(usersCount, visitTimes);
        return "OK";
    }
    
    @GetMapping("/visit/log")
    public Object userVisitLog(@RequestParam(defaultValue = "1")int uid) {
        List<Pv> list = pvMapper.selectVisitLog(uid);
        log.info("用户访问uid={},logs:{}", uid, JSON.toJSONString(list));
        return list;
    }
    
    
}
