package com.sharding.jdbc.demo2.service;
/**
* @comments
* @author  zg
* @date 2021年1月1日
*/
public interface UserService {

    void createUsers(int userCount);

    void randomUserVisit(int maxUserCount, int visitTimes);

}
