package com.sharding.jdbc.demo2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@MapperScan("com.sharding.jdbc.demo2.mapper")
@SpringBootApplication
public class ShardingJdbcDemo2Application {
    
    private static Class<?> cla = ShardingJdbcDemo2Application.class;

	public static void main(String[] args) {
		SpringApplication.run(cla, args);
		log.info("ShardingJdbcDemo2Application starting......");
	}
}
