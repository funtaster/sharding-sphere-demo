package com.sharding.jdbc.demo2.util;
/**
* @comments
* @author  zg
* @date 2021年1月2日
*/
public class ShardingUtils {
    
    public static int pv_segmentSize = 30;
    
    /**
     * 得到分表的表名
     * @author zg
     * @date 2021年1月2日 
     * @param tableName
     * @param segmentSize
     * @param uid
     * @return
     */
    public static String getShardingTableByRange(String tableName, int segmentSize, int uid) {
        int tableIdx = uid / segmentSize;
        return tableName +"_"+ tableIdx;
    }
}
