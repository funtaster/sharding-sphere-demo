package com.sharding.jdbc.demo2.mapper;

import java.util.List;

import com.sharding.jdbc.demo2.model.Pv;

/**
 * PvMapper继承基类
 */
public interface PvMapper {
    
    /**
     * 查询表是否存在
     * @author zg
     * @date 2021年1月2日 
     * @param tableName
     * @return 1：是，0：否
     */
    int isTableExist(String tableName);

    /**
     * 创建表
     * @author zg
     * @date 2021年1月2日 
     * @param tableName
     * @return
     */
    int createTable(String tableName);

    int insert(Pv pv);

    List<Pv> selectVisitLog(int uid);
    
}