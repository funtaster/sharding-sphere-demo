package com.sharding.jdbc.demo2.model;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 
 * 用户访问记录
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Pv implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer id;

    private Integer uid;

    private String uri;

    private Date cttime;

}