# sharding-sphere-demo

#### 项目介绍
Spring boot 2.2.6 + Sharding-Sphere 4.0.1

只涉及到分表，根据用户取模策略，对用户表以及用户订单表进行分表

演示的功能：
1. 创建用户，根据生成的用户id 把用户信息sharding到表 user_{n} 中。 
2. 创建订单，根据用户id把用户的订单sharding到表 order_{n}中。
3. 查询用户信息。
4. 查询订单信息。
5. 事务


分表策略：

1. 用户id由雪花算法生成
2. 用户表拆分成3张：user_0, user_1, user_2
3. 订单表拆分成3表: order_0, order_1, order_2


